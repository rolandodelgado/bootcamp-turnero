"""
URL configuration for turnero project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from appTurnero import views
#IMPORTAR SETTINGS PARA VARIABLE DEBUG
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('administrar/', admin.site.urls),
    path('', views.inicio, name='Inicio'),
    path('clientes/',views.clientesView,name='Clientes'),
    path('servicios/',views.serviciosView,name='Servicios'),
    path('estados/',views.estadosView,name='Estados'),
    path('niveles/',views.nivelesView,name='Niveles'),
    path('turnos/',views.turnosView,name='Turnos'),
    path('usuarios/',views.usuariosView,name='Usuarios'),
    path('cambios/',views.cambiosView,name='Cambios'),
    path('inicio/',views.inicio,name='Index'),
    path('lista_clientes/',views.lista_clientes,name="Lista Clientes"),
    path('lista_turnos/',views.lista_turnos,name="Lista Turnos"),
    path('lista_usuarios/',views.lista_usuarios,name="Lista Usuarios")
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)