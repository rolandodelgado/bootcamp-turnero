from django import forms
import datetime
from .models import CambiosEstados, Servicios, Usuarios, Clientes, Estados, Niveles, Turnos

#Clase del formulario para Usuarios
class UsuarioForm(forms.Form):
    usuario = forms.CharField(max_length=15, required=True)
    nombre = forms.CharField(max_length=128, required=True)
    apellido = forms.CharField(max_length=128, required=True)
    email = forms.EmailField(max_length=120, required=True)
    password = forms.CharField(label="Contraseña", max_length=120, required=True)
    fecha_alta = forms.DateField(label="Fecha", initial=datetime.date.today, required=True)
    activo = forms.BooleanField(label="Estado Activo", initial=True,required=True)

#Personalizar formulario models en admin
class UsuarioModelForm(forms.ModelForm):
    class Meta:
        model = Usuarios
        fields = ['usuario', 'nombre', 'apellido', 'email', 'password', 'activo']
        widgets = {'password': forms.PasswordInput()}

#Clase del formulario para Clientes
class ClientesForm(forms.ModelForm):
    class Meta:
        model = Clientes
        fields = '__all__'
        widgets = {
            'fecha_nacimiento': forms.DateInput(attrs={'type': 'date'}),
        }
    
#Personalizar formulario models en admin
class ClienteModelForm(forms.ModelForm):
    class Meta:
        model = Clientes
        #fields = ['nombre']
        fields = ['nombre', 'ci', 'ruc', 'fecha_nacimiento', 'corporativo', 'discapacitado', 'telefono', 'sexo', 'activo', 'id_nivel']


class ServiciosForm(forms.ModelForm):
    class Meta:
        model = Servicios
        fields = ['descripcion', 'activo']
        labels = {
            'descripcion': 'Descripción del servicio',
            'activo': 'Servicio activo'
        }
        widgets = {
            'descripcion': forms.TextInput(attrs={'class': 'form-control'}),
            'activo': forms.CheckboxInput(attrs={'class': 'form-check-input'})
        }

class EstadosForm(forms.ModelForm):
    class Meta:
        model = Estados
        fields = '__all__'

class NivelesForm(forms.ModelForm):
    class Meta:
        model = Niveles
        fields = '__all__'

class TurnosForm(forms.ModelForm):
    class Meta:
        model = Turnos
        fields = '__all__'

class CambiosEstadosForm(forms.ModelForm):
    class Meta:
        model = CambiosEstados
        fields = '__all__'
