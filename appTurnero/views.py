from django.shortcuts import render
#importar modelo y formulario
from appTurnero.models import Usuarios, Clientes, Estados, Niveles, Turnos, CambiosEstados
from appTurnero.forms import ClientesForm, UsuarioModelForm,ClienteModelForm,UsuarioForm,EstadosForm,NivelesForm,TurnosForm,CambiosEstadosForm,ServiciosForm
#importar configuracion para  imprimir la base
from django.conf import settings

# Create your views here.

def inicio(request):
    
    #imprimir variable de la base
    #print(settings.BASE_DIR)

    #formulario usuarios
        #crear objeto de otra forma, si tenes valores en partes diferentes, para validaciones
        # se puede usar en for
        # # obj = Usuarios()
        # obj.usuario=aux_usuario
        # obj.save()
        
    # #Formulario clientes
    # formCli = ClienteForm(request.POST or None)
    # if formCli.is_valid():
    #     # declarar var
    #     form_data = formCli.cleaned_data
        
    #     # // imprime en fomato JSON
    #     #print(form_data)
        
    #     # //funcion get para obtener el dato en limpio
    #     #print(form_data.get("usuario"))

    #     #declarar variables auxiliares
    #     aux_nombre = form_data.get("nombre")
    #     aux_ci = form_data.get("ci")
    #     aux_ruc = form_data.get("ruc")
    #     aux_fecha_nacimiento = form_data.get("fecha_nacimiento")
    #     aux_corporativo = form_data.get("corporativo")
    #     aux_discapacitado = form_data.get("discapacitado")
    #     aux_telefono = form_data.get("telefono")
    #     aux_sexo = form_data.get("sexo")
    #     aux_email = form_data.get("email")
    #     aux_id_nivel = form_data.get("id_nivel")
    #     aux_user_insert = form_data.get("user_insert")
    #     aux_activo = form_data.get("activo")

    #     #crear objeto en una linea con try catch
    #     try:
    #         obj = ClienteModelForm.objects.create(nombre = aux_nombre,
    #         ci = aux_ci,
    #         ruc = aux_ruc,
    #         fecha_nacimiento = aux_fecha_nacimiento,
    #         corporativo = aux_corporativo,
    #         discapacitado = aux_discapacitado,
    #         telefono = aux_telefono,
    #         sexo = aux_sexo,
    #         email = aux_email,
    #         id_nivel = aux_id_nivel,
    #         user_insert = aux_user_insert,
    #         activo = aux_activo)
    #         obj.save()
    #     except Exception as e:
    #         print("error:", e)
        
    context = {
        #variables para titulos 
        "titulo":'Sistema Turnero',
        }
    return render(request,'index.html',context)

def usuariosView(request):
    form = UsuarioModelForm(request.POST or None)
    if form.is_valid():
        # declarar var
        form_data = form.cleaned_data
        
        # // imprime en fomato JSON
        #print(form_data)
        
        # //funcion get para obtener el dato en limpio
        #print(form_data.get("usuario"))

        #declarar variables auxiliares
        aux_usuario = form_data.get("usuario")
        aux_nombre = form_data.get("nombre")
        aux_apellido = form_data.get("apellido")
        aux_email = form_data.get("email")
        aux_password = form_data.get("password")
        aux_fecha_alta = form_data.get("fecha_alta")
        aux_activo = form_data.get("activo")

        #crear objeto en una linea con try catch
        try:
            obj = Usuarios.objects.create(usuario = aux_usuario,
            nombre = aux_nombre,
            apellido = aux_apellido,
            email = aux_email,
            password = aux_password,
            fecha_alta = aux_fecha_alta,
            activo = aux_activo)
            obj.save()
        except Exception as e:
            print("error:", e)

    context = {   
        #variables para titulos
        "titulo":'Registro de Usuarios',
        #variables de formularios
        "form_usu":form
    }
    return render(request,'usuarios.html',context)


def clientesView(request):
    if request.method == 'POST':
        form = ClientesForm(request.POST)
        if form.is_valid():
            form.save()
            #return redirect('clientes_list')
    else:
        form = ClientesForm()

    context = {   
        #variables para titulos
        "titulo":'Registro de Clientes',
        #variables de formularios
        "form_cli":form
    }
    return render(request, 'clientes.html', context)

# def clientesView(request):
#         #Formulario clientes
#     form = ClienteForm(request.POST or None)
#     if form.is_valid():
#         # declarar var
#         form_data = form.cleaned_data
        
#         # // imprime en fomato JSON
#         #print(form_data)
        
#         # //funcion get para obtener el dato en limpio
#         #print(form_data.get("usuario"))

#         #declarar variables auxiliares
#         aux_nombre = form_data.get("nombre")
#         aux_ci = form_data.get("ci")
#         aux_ruc = form_data.get("ruc")
#         aux_fecha_nacimiento = form_data.get("fecha_nacimiento")
#         aux_corporativo = form_data.get("corporativo")
#         aux_discapacitado = form_data.get("discapacitado")
#         aux_telefono = form_data.get("telefono")
#         aux_sexo = form_data.get("sexo")
#         aux_email = form_data.get("email")
#         aux_id_nivel = form_data.get("id_nivel")
#         aux_user_insert = form_data.get("user_insert")
#         aux_activo = form_data.get("activo")

#         #crear objeto en una linea con try catch
#         try:
#             obj = Clientes.objects.create(nombre = aux_nombre,
#             ci = aux_ci,
#             ruc = aux_ruc,
#             fecha_nacimiento = aux_fecha_nacimiento,
#             corporativo = aux_corporativo,
#             discapacitado = aux_discapacitado,
#             telefono = aux_telefono,
#             sexo = aux_sexo,
#             email = aux_email,
#             id_nivel = aux_id_nivel,
#             user_insert = aux_user_insert,
#             activo = aux_activo)
#             obj.save()
#         except Exception as e:
#             print("error:", e)
#     context = {
#         "titulo":'Registro de Clientes',
#         "form_cli":form
#     }
#     return render(request,'clientes.html',context)

def serviciosView(request):
        #Formulario servicios
    form = ServiciosForm(request.POST or None)
    if form.is_valid():
        # declarar var
        form_data = form.cleaned_data
        
        #declarar variables auxiliares
        
        #crear objeto en una linea con try catch
        
    context = {
        "titulo":'Registro de Servicios',
        "form_ser":form
    }
    return render(request,'servicios.html', context)


def estadosView(request):
    form = EstadosForm(request.POST or None)
    if form.is_valid():
        form.save()
        #return redirect('estados_list')
    
    context = {
        "titulo":'Registro de Estados',
        "form_est":form
    }
    return render(request, 'estados.html', context)

def nivelesView(request):
    form = NivelesForm(request.POST or None)
    if form.is_valid():
        form.save()
        #return redirect('niveles_list')
    
    context = {
        "titulo":'Registro de Niveles',
        "form_niv":form
    }
    return render(request, 'niveles.html', context)

def turnosView(request):
    form = TurnosForm(request.POST or None)
    if form.is_valid():
        form.save()
        #return redirect('turnos_list')

    context = {
        "titulo":'Registro de Turnos',
        "form_tur":form
    }
    return render(request, 'turnos.html', context)

def cambiosView(request):
    form = CambiosEstadosForm(request.POST or None)
    if form.is_valid():
        form.save()
        #return redirect('cambios_estados_list')

    context = {
        "titulo":'Registro de Cambios de Estados',
        "form_cam":form
    }
    return render(request, 'cambios.html', context)

#Vista para mostrar la lista de clientes:
def lista_clientes(request):
    clientes = Clientes.objects.all()
    return render(request, 'lista_clientes.html', {'clientes': clientes})

#Vista para mostrar la lista de turnos:
def lista_turnos(request):
    turnos = Turnos.objects.all()
    return render(request, 'lista_turnos.html', {'turnos': turnos})

#Vista para mostrar la lista de usuarios:
def lista_usuarios(request):
    usuarios = Usuarios.objects.all()
    return render(request, 'lista_usuarios.html', {'turnos': usuarios})