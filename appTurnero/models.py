import datetime
from django.db import models

# Create your models here.
class Usuarios(models.Model):
    id_usuario = models.AutoField(primary_key=True, db_comment='clave primaria de usuarios')
    usuario = models.CharField(max_length=15, db_comment='usuario', unique=True)
    nombre = models.CharField(max_length=128, db_comment='nombre del usuario')
    apellido = models.CharField(max_length=128, db_comment='nombre del usuario')
    email = models.CharField(max_length=120, db_comment='direccion de correo electronico del usuario')
    password = models.CharField(max_length=120, db_comment='contraseña del usuario')
    fecha_alta = models.DateField(default=datetime.date.today, db_comment='fecha de alta del usuario')
    activo = models.BooleanField(default=True, db_comment='Es un usuario activo?')

    def __str__(self):
        return self.usuario

    class Meta:
        db_table = 'usuarios'
        db_table_comment = 'Tabla de usuarios'


class Servicios(models.Model):
    id_servicio = models.SmallAutoField(primary_key=True, db_comment='tabla de servicios de la empresa')
    descripcion = models.CharField(max_length=254, unique=True, db_comment='descripcion del servicio')
    activo = models.BooleanField(default=True, db_comment='Es un servicio activo?')
    user_insert = models.ForeignKey('Usuarios', models.DO_NOTHING, db_column='user_insert', db_comment='Usuario que cargo')
    fecha_alta = models.DateField(default=datetime.date.today, db_comment='fecha de creacion')

    def __str__(self):
        return self.descripcion

    class Meta:
        db_table = 'servicios'
        db_table_comment = 'TABLA DE SERVICIOS'


class Estados(models.Model):
    id_estado = models.SmallAutoField(primary_key=True, db_comment='clave primaria de la tabla de estados')
    estado_descripcion = models.CharField(max_length=48, unique=True, db_comment='descripcion del estado')
    activo = models.BooleanField(default=True, db_comment='Es un estado activo?')

    def __str__(self):
        return self.estado_descripcion

    class Meta:
        db_table = 'estados'
        db_table_comment = 'Tabla de estados'


class Niveles(models.Model):
    TOP = "A"
    MIDDLE = "M"
    BOTTOM = "B"
    PRIORIDAD_CHOICES = [
        (TOP, "Alta"),
        (MIDDLE, "Media"),
        (BOTTOM, "Baja"),
    ]
    id_nivel = models.SmallAutoField(primary_key=True, db_comment='clave primaria del nivel')
    descripcion = models.CharField(max_length=24, db_comment='descripcion del nivel')
    prioridad = models.CharField(max_length=5, choices=PRIORIDAD_CHOICES, db_comment='escala de prioridad')
    activo = models.BooleanField(default=True, db_comment='Estado del nivel')

    def __str__(self):
        return self.descripcion

    class Meta:
        db_table = 'niveles'
        db_table_comment = 'tabla donde se guardan los niveles de prioridad del cliente'


class Clientes(models.Model):
    MALE = "M"
    FEMALE = "F"
    SEXO_CHOICES = [
        (MALE, "Masculino"),
        (FEMALE, "Femenino"),
    ]
    id_cliente = models.SmallAutoField(primary_key=True, db_comment='clave primaria de la tabla clientes')
    nombre = models.CharField(max_length=128, db_comment='nombre del cliente')
    ci = models.IntegerField(blank=True, null=True, unique=True, db_comment='numero de documento del cliente')
    ruc = models.CharField(blank=True, null=True, db_comment='numero de ruc del cliente')
    fecha_nacimiento = models.DateField(db_comment='fecha de nacimiento del cliente')
    corporativo = models.BooleanField(db_comment='Es un cliente corporativo?')
    discapacitado = models.BooleanField(db_comment='Es un cliente con capacidades diferentes?')
    telefono = models.CharField(max_length=48, blank=True, null=True, db_comment='numero de contacto')
    sexo = models.CharField(max_length=9, choices=SEXO_CHOICES, db_comment='Sexo del cliente')
    activo = models.BooleanField(default=True, db_comment='Es un cliente activo?')
    id_nivel = models.ForeignKey('Niveles', models.DO_NOTHING, db_column='id_nivel', db_comment='clave primaria del nivel')
    user_insert = models.ForeignKey('Usuarios', models.DO_NOTHING, db_column='user_insert', db_comment='Usuario que cargo')
    fecha_alta = models.DateField(default=datetime.date.today, db_comment='fecha de creacion')

    def __str__(self):
        return self.nombre

    class Meta:
        db_table = 'clientes'
        db_table_comment = 'tabla de clientes'


class Turnos(models.Model):
    id_turno = models.AutoField(primary_key=True, db_comment='clave primaria de la tabla turnos')
    id_servicio = models.ForeignKey(Servicios, models.DO_NOTHING, db_column='id_servicio', db_comment='tabla de servicios de la empresa')
    id_cliente = models.ForeignKey(Clientes, models.DO_NOTHING, db_column='id_cliente', db_comment='clave primaria de la tabla clientes')
    id_estado = models.ForeignKey(Estados, models.DO_NOTHING, db_column='id_estado', db_comment='clave primaria de la tabla de estados')
    descripcion = models.CharField(max_length=240, blank=True, null=True, db_comment='detalle del turno, comentarios, etc')
    user_insert = models.ForeignKey('Usuarios', models.DO_NOTHING, db_column='user_insert', db_comment='Usuario que cargo el nivel')
    fecha_inicio = models.DateField(default=datetime.date.today, db_comment='Fecha de solicitud')
    fecha_alta = models.DateField(blank=True, null=True, db_comment='fecha de cierre del turno')

    def __str__(self):
        return self.id_turno

    class Meta:
        db_table = 'turnos'
        db_table_comment = 'Tabla donde se registran todos los turnos solicitados'


class CambiosEstados(models.Model):
    id_cambio_estado = models.AutoField(primary_key=True, db_comment='clave primaria de la tabla cambio de estado')
    id_usuario = models.ForeignKey('Usuarios', models.DO_NOTHING, db_column='id_usuario', db_comment='clave primaria de usuarios')
    id_turno = models.ForeignKey('Turnos', models.DO_NOTHING, db_column='id_turno', db_comment='clave primaria de la tabla turnos')
    fecha_cambio = models.DateField(default=datetime.date.today, db_comment='fecha del cambio de estado')
    descripcion = models.CharField(max_length=120, blank=True, null=True, db_comment='COMENTARIO DEL CAMBIO DE ESTADO')
    estado_inicial = models.SmallIntegerField(db_comment='estado antes del cambio')
    estado_final = models.SmallIntegerField(db_comment='estado despues del cambio')

    def __str__(self):
        return self.id_turno

    class Meta:
        db_table = 'cambios_estados'
        db_table_comment = 'Tabla de historial de cambio de estados del turno'