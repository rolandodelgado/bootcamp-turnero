from django.contrib import admin
from . import models
from .forms import UsuarioModelForm

#Register your models here.
class CambiosEstadosAdmin(admin.ModelAdmin):
    list_display = []
    list_filter = []
    list_editable = []
    search_fields = []


class ClientesAdmin(admin.ModelAdmin):
    list_display = ["ci","nombre","ruc","corporativo","discapacitado","telefono","sexo","activo","id_nivel"]
    list_filter = ["corporativo","discapacitado","sexo","activo","id_nivel"]
    list_editable = ["corporativo","discapacitado","sexo","activo","id_nivel"]
    search_fields = ["ci","nombre","ruc"]
    list_per_page = 10


class EstadosAdmin(admin.ModelAdmin):
    list_display = ["estado_descripcion","activo"]
    list_filter = ["activo"]
    list_editable = ["activo"]
    search_fields = []


class NivelesAdmin(admin.ModelAdmin):
    list_display = ["descripcion","prioridad","activo"]
    list_filter = ["prioridad","activo"]
    list_editable = ["prioridad","activo"]
    search_fields = ["descripcion"]


class ServiciosAdmin(admin.ModelAdmin):
    list_display = ["descripcion","activo"]
    list_filter = ["descripcion"]
    list_editable = ["activo"]
    search_fields = ["descripcion"]


class TurnosAdmin(admin.ModelAdmin):
    list_display = ["id_cliente","id_servicio","id_estado","descripcion","user_insert","fecha_inicio","fecha_alta",]
    list_filter = ["id_cliente","id_servicio","id_estado"]
    list_editable = ["id_estado","fecha_alta"]
    search_fields = ["id_cliente","id_servicio","id_estado"]


class UsuariosAdmin(admin.ModelAdmin):
    list_display = ["__str__","nombre","apellido","email","activo","fecha_alta"]
    list_filter = ["activo","fecha_alta"]
    list_editable = ["activo"]
    search_fields = ["usuario","nombre","apellido"]
    #Personalizar form models
    #form=UsuarioModelForm
    class Meta:
         model = models.Usuarios


admin.site.register(models.CambiosEstados, CambiosEstadosAdmin)
admin.site.register(models.Clientes, ClientesAdmin)
admin.site.register(models.Estados, EstadosAdmin)
admin.site.register(models.Niveles, NivelesAdmin)
admin.site.register(models.Servicios, ServiciosAdmin)
admin.site.register(models.Turnos, TurnosAdmin)
admin.site.register(models.Usuarios, UsuariosAdmin)